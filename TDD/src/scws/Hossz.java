package scws;

public class Hossz {
	double val;
	String me;
	
	public Hossz() {};
	
	public Hossz(double val, String me) throws SajatException {
		switch(me) {
		case "m": break;
		case "cm": val=val/100; break;
		case "km": val=val*1000; break;
		case "in": val=val*2.54/100; break;
		case "ft": val=val*12*2.54/100; break;
		case "yd": val=val*3*12*2.54/100; break;
		case "mile": val=val*3*12*2.54*1760/100; break;
		case "nm": val=val*1852; break;
		default: me=null; break; 
	    }
		
        if(me == null) {	
			throw new SajatException("Invalid me");
		}
		else {
		this.val=val;
		this.me=me;
		}
	}
	
	public int compareTo(Hossz o) {
		int eredmeny = 0;
		if(o.me == null || me == null) {
			try {
				throw new SajatException("Invalid me");
			} catch (SajatException e) {
			}
		}
		else {
		Double kivonas = o.val - val;
		eredmeny = kivonas.intValue();
		}
		return Math.abs(eredmeny);
	}
	
	public double addHossz(Hossz hossz) {	
		return hossz.val+val;
	}
	
	public double minusHossz(Hossz hossz) {
		return Math.abs(hossz.val-val);
	}
	
	public void multiplication(double scalar) {
		val*=scalar;
	}
	
	public double divisionSebesseg(Sebesseg sebesseg) {
		return val/sebesseg.val;
	}
}
