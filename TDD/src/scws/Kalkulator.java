package scws;

public class Kalkulator {

	Hajo hajo;
	public Kalkulator(Hajo hajo) {
		this.hajo=hajo;
	}
	public String utkozik_e(Hajo masik, Pozicio p, int irany) throws wrongTimeException {
		double vx, vy,t,Y,T, dt, dT;
		Ido ddt,tt;
		Ido ddT,TT;
		
		IdoIntervallum miVallum;
		IdoIntervallum masikVallum;
		
		if((p.x.val>0 && irany>=0 && irany<=180) ||  (p.x.val<0 && irany>=180 && irany<=360)) {
			return "Nincs �tk�z�s!!";
		}
		else{
			vx=masik.sebesseg.val*Math.cos(90-irany);
			vy=masik.sebesseg.val*Math.sin(90-irany);
			t=-(p.x.val/vx);
			Y=p.y.val+vy*t;
			T=Y/hajo.sebesseg.val; //sec
			
			dt=3.5*masik.hossz.val/masik.sebesseg.val; //  m�sik hajo
			ddt = new Ido(converttoIdo(dt));
			tt = new Ido(converttoIdo(t));
			
			String masikPlus = tt.trueAdd(ddt);
			String masikMinus = tt.trueMinus(ddt);
			
			Ido masikEnd = new Ido(masikPlus);
			Ido masikStart = new Ido(masikMinus);
			
			masikVallum = new IdoIntervallum(masikStart,masikEnd);
			
			dT = 3.5 * hajo.hossz.val / hajo.sebesseg.val;		 // mi hajonk
			ddT = new Ido(converttoIdo(dT));
			TT = new Ido(converttoIdo(T));
			
			String sajatplus = tt.trueAdd(ddT);
			String sajtminus = tt.trueMinus(TT);
			
			Ido sajatEnd = new Ido(sajatplus);
			Ido sajatStart = new Ido(sajtminus);
			
			miVallum = new IdoIntervallum(sajatStart,sajatEnd);
			
			//System.out.println(miVallum.idoEgyHour+":"+miVallum.idoEgyMin+":"+miVallum.idoEgySec+","+miVallum.idoKettoHour+":"+miVallum.idoKettoMin+":"+miVallum.idoKettoSec);
		    //System.out.println(masikVallum.idoEgyHour+":"+masikVallum.idoEgyMin+":"+masikVallum.idoEgySec+","+masikVallum.idoKettoHour+":"+masikVallum.idoKettoMin+":"+masikVallum.idoKettoSec);
			
			int eredmeny = miVallum.metsze_e(masikVallum);
			
			if (eredmeny == 0) {
				return "Nincs �tk�z�s!!";
			}
			else {
				if((irany>=270 && irany <= 360) || (irany>=0 && irany<=90)) {
					if(p.x.val>0) {
						return("� mehet,de mi lass�tsunk");
					}
					else {
						return("Mi mehet�nk,de mi lass�tsunk");	
					}
				}
				else {
					if(masik.tomeg.val>hajo.tomeg.val) {
						
						return("Mi mehet�nk,de figyelj�nk r�");
						
					}
					else {
						return("� mehet,de lass�tsunk");
					}
				}
			}
		}
	}
	
	public static String converttoIdo(double sec) {
		
		int percek = (int) (sec / 60);
		int masodpercek = (int) (sec%60);
		
		int orak = percek / 60;
		percek = percek % 60;

		Ido eredmeny = new Ido(orak+":"+percek+":"+masodpercek);	
		
		return (orak+":"+percek+":"+masodpercek);
	}
}
