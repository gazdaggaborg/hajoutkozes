package scws;

public class IdoIntervallum {
	

	
	int idoEgySec;
	int idoEgyMin;
	int idoEgyHour;
	
	int idoKettoSec;
	int idoKettoMin;
	int idoKettoHour;

	

	public IdoIntervallum(Ido ido, Ido ido2) {
		
		idoEgySec = ido.sec;
		idoEgyMin = ido.min;
		idoEgyHour = ido.hour;
		
				
		idoKettoSec = ido2.sec;
		idoKettoMin = ido2.min;
		idoKettoHour = ido2.hour;
		
		
	}
	
	
	




	public int metsze_e(IdoIntervallum idoIntervallum) {
		
		
		Ido idoEgyEgy = new Ido(this.idoEgyHour+":"+this.idoEgyMin+":"+this.idoEgySec);
		Ido idoEgyKetto = new Ido(this.idoKettoHour+":"+this.idoKettoMin+":"+this.idoKettoSec);
		
		Ido idoKettoEgy = new Ido(idoIntervallum.idoEgyHour+":"+idoIntervallum.idoEgyMin+":"+idoIntervallum.idoEgySec);
		Ido idoKettoKetto = new Ido(idoIntervallum.idoKettoHour+":"+idoIntervallum.idoKettoMin+":"+idoIntervallum.idoKettoSec);
		
		
		
		//vallum_hanyadik_(also,felso)
		double v_1_1 = idoEgyEgy.convertToMin();
		double v_1_2 = idoEgyKetto.convertToMin();
		
		//System.out.println(v_1_1+","+v_1_2);
		
		double v_2_1 = idoKettoEgy.convertToMin();
		double v_2_2 = idoKettoKetto.convertToMin();
		
		//System.out.println(v_2_1+","+v_2_2);
		
		if(((v_2_2>v_1_1) && (v_2_2<v_1_2)) || (((v_2_1>v_1_1) && (v_2_1<v_1_2)) &&
				((v_2_2>v_1_1) && (v_2_2<v_1_2))) && ((v_2_1>v_1_1) && (v_2_1<v_1_2))
				|| (((v_2_1<v_1_1) && (v_1_1<v_2_2)) && ((v_1_2>v_2_1) && (v_1_2<v_2_2)))
				
				) {
			
			return 1;
		}
		
		
		
		return 0;
		
	}







	public String addIdo(Ido ido) {

		Ido idoEgyEgy = new Ido(this.idoEgyHour+":"+this.idoEgyMin+":"+this.idoEgySec);
		Ido idoEgyKetto = new Ido(this.idoKettoHour+":"+this.idoKettoMin+":"+this.idoKettoSec);
		
		
		String also = idoEgyEgy.timeMinus(ido);
		String felso = idoEgyKetto.timePlus(ido);
		
	
		
		
		return (also+","+felso);
	}







	public String trueAddIdo(Ido ido) throws wrongTimeException {
		
		Ido idoEgyEgy = new Ido(this.idoEgyHour+":"+this.idoEgyMin+":"+this.idoEgySec);
		Ido idoEgyKetto = new Ido(this.idoKettoHour+":"+this.idoKettoMin+":"+this.idoKettoSec);
		
		
		String also = idoEgyEgy.trueMinus(ido);
		String felso = idoEgyKetto.trueAdd(ido);
		
		
		
		return (also+","+felso);
	}

}
