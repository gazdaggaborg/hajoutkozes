package scws;

import java.text.DecimalFormat;

public class Ido {
	
	int sec;
	int min;
	int hour;

	public Ido(String ido) {
		
		
		String[] idoParts;
		idoParts = ido.trim().split("\\:");
		
		this.sec = Integer.parseInt(idoParts[2]);
		this.min = Integer.parseInt(idoParts[1]);
		this.hour = Integer.parseInt(idoParts[0]);
		

		
	}
	
	public double convertToMin() {
		
		double secToMin =  this.sec / 60d;
		
		double hourToMin = this.hour * 60;
		
		double osszmin =  secToMin + this.min + hourToMin;
		
	//	double value =Double.parseDouble(new DecimalFormat("##.##").format(osszmin));
		
		return osszmin;
		
		
	}
	
	public double compareTo(Ido o) {
		double value = 0;
		
		double egyik = o.convertToMin();
		double masik = convertToMin();
		value = egyik - masik;
		return Math.abs(value);
	}

	public String timePlus(Ido o) {
		
		int allHour = this.hour + o.hour;
		int allMin = this.min + o.min;
		int allSec = this.sec + o.sec;
		
		
		if(allHour >= 24) {
			
			allHour -= 24;
		}
		
		if (allMin >= 60) {
			
			int howManyHour = allMin / 60;
			allHour += howManyHour;
			allMin = allMin - (howManyHour * 60);
			
		}
		
		if (allSec >= 60) {
			
			int howManyMin = allSec / 60;
			allMin += howManyMin;
			allSec = allSec - (howManyMin * 60);
			
		}
				
		
		return (allHour+":"+allMin+":"+allSec);
	}
	public String timeMinus(Ido o) {
		
		int allHour = this.hour - o.hour;
		int allMin = this.min - o.min;
		int allSec = this.sec - o.sec;
		
		
		if(allHour < 0) {
			
			allHour = 24 - Math.abs(allHour);
		}
		
		if (allMin < 0) {
			
			allMin = 60 - Math.abs(allMin);
			allHour --;
			
		}
		
		if (allSec <0) {
			
			allSec = 60 - Math.abs(allSec);
			allMin -- ;
			
		}
				
		
		return (allHour+":"+allMin+":"+allSec);
	}
	
	
	public String trueAdd(Ido o) {
		
		int allHour = this.hour + o.hour;
		int allMin = this.min + o.min;
		int allSec = this.sec + o.sec;
		
		
		if (allMin >= 60) {
			
			int howManyHour = allMin / 60;
			allHour += howManyHour;
			allMin = allMin - (howManyHour * 60);
			
		}
		
		if (allSec >= 60) {
			
			int howManyMin = allSec / 60;
			allMin += howManyMin;
			allSec = allSec - (howManyMin * 60);
			
		}
		
		
		return (allHour+":"+allMin+":"+allSec);
	}

	public String trueMinus(Ido o) throws wrongTimeException {
		
		int allHour = 0;
		int allMin = 0 ;
		int allSec = 0;
		
		
/*		if(this.hour < o.hour) {
			
			throw new wrongTimeException("Wrong Time");
			
			
		}
		
		else { */
		 allHour = this.hour - o.hour;
		 allMin = this.min - o.min;
		 allSec = this.sec - o.sec;
		
		
		if (allMin < 0) {
			
			allMin = 60 - Math.abs(allMin);
			allHour --;
			
		}
		
		if (allSec <0) {
			
			allSec = 60 - Math.abs(allSec);
			allMin -- ;
			
		}
		
	//	}
		
		return (allHour+":"+allMin+":"+allSec);
	}
	
}
