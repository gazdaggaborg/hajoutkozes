package scws;

public class Pozicio {

	Hossz x,y;	
	
	public Pozicio(Hossz x,Hossz y) throws FailedPositionConstructionException{
		
		if(x==null || y == null) {
			throw new FailedPositionConstructionException("Koordinįta null!");
		}
		
		this.x=x;
		this.y=y;
	}

	public Pozicio() {
		// TODO Auto-generated constructor stub
	}
}
