package scws;

public class FailedPositionConstructionException extends Exception {

	private static final long serialVersionUID = 1L;

	public FailedPositionConstructionException(String msg) {
		super(msg);
	}
}
