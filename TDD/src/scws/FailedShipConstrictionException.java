package scws;

public class FailedShipConstrictionException extends Exception {
	
	public FailedShipConstrictionException(String msg) {
		super(msg);
	}

}
