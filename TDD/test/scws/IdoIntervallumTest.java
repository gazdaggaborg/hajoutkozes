package scws;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class IdoIntervallumTest {

	@Test
	public void initTest()  {
		
		assertNotNull(new IdoIntervallum((new Ido("12:16:22")),new Ido("12:42:44")));
	}
	
	// 1 metszi, 0 nem
	@Test
	public void metsze_eTest()  {
		
		assertEquals(1,new IdoIntervallum((new Ido("12:16:22")),new Ido("22:42:44")).
				metsze_e(new IdoIntervallum((new Ido("13:16:22")),new Ido("15:42:44"))));
		
		assertEquals(0,new IdoIntervallum((new Ido("12:16:22")),new Ido("22:42:44")).
				metsze_e(new IdoIntervallum((new Ido("13:16:22")),new Ido("23:42:44"))));
		
		assertEquals(1,new IdoIntervallum((new Ido("12:16:22")),new Ido("22:42:44")).
				metsze_e(new IdoIntervallum((new Ido("10:16:22")),new Ido("19:42:44"))));
		
		assertEquals(0,new IdoIntervallum((new Ido("12:16:22")),new Ido("22:42:44")).
				metsze_e(new IdoIntervallum((new Ido("10:16:22")),new Ido("11:42:44"))));
	}



	@Test
	public void newVallumTest() {
		
		
		assertEquals("11:4:21,14:54:45",new IdoIntervallum((new Ido("12:16:22")),new Ido("13:42:44")).addIdo(new Ido("1:12:01")));
		
	}

	@Test
	public void trueVallumTest() throws wrongTimeException {
				
		assertEquals("11:4:21,25:54:45",new IdoIntervallum((new Ido("12:16:22")),new Ido("24:42:44")).trueAddIdo(new Ido("1:12:01")));
		
	}
	


	
	
	
}
