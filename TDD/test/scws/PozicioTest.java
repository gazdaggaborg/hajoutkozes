package scws;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class PozicioTest {

	@Test
	public void testNewPozicio() throws Exception {
		
		assertNotNull(new Pozicio(new Hossz(40,"m"), new Hossz(50,"m")));
		}
	
	@Test(expected = FailedPositionConstructionException.class )
	public void testInvalidConstruct () throws FailedPositionConstructionException, SajatException {
		
		Pozicio pos = new Pozicio(new Hossz(30,"m"),null);

	}
	
	@Test(expected = Exception.class )
	public void testInvalidConstruct2 () throws Exception {
		
		Pozicio pos = new Pozicio(null, new Hossz(50,"m"));

	}
	
	@Test(expected = Exception.class )
	public void testInvalidConstruct3 () throws Exception {
		
		Pozicio pos = new Pozicio();

	}
}
