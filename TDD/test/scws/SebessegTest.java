package scws;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SebessegTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void initTest() throws SajatException {
		
		assertNotNull(new Sebesseg(5.0, "m/s"));
		
	}
	//különbség m/s-ben
	@Test
	public void compareTest() throws SajatException {
		
		

		assertEquals(0,new Sebesseg(1.0, "m/s").compare(new Sebesseg(3.6, "km/h")));
		assertEquals(0,new Sebesseg(1.0, "m/s").compare(new Sebesseg(2.23694, "mi/h")));
		assertEquals(0,new Sebesseg(1.0, "m/s").compare(new Sebesseg(1.94384, "knot")));
		

		assertEquals(0,new Sebesseg(1.0, "km/h").compare(new Sebesseg(0.621371, "mi/h")));
		assertEquals(0,new Sebesseg(1.0, "km/h").compare(new Sebesseg(0.539957, "knot")));
		

		assertEquals(0,new Sebesseg(1.0, "mi/h").compare(new Sebesseg(0.868976, "knot")));
		

		
		
		
	}
	//méterben kapod az eredmenyt
	@Test
	public void tavTest() throws SajatException {
		
		assertEquals(3600,new Sebesseg(1.0, "m/s").toTav(new Ido("01:0:00")),0.0);
		assertEquals(1800,new Sebesseg(3.6, "km/h").toTav(new Ido("00:30:00")),0.0);
		assertEquals(1800,new Sebesseg(2.237, "mi/h").toTav(new Ido("00:30:00")),0.0);
		assertEquals(1800,new Sebesseg(1.944, "knot").toTav(new Ido("00:30:00")),0.0);
		
	}
}
