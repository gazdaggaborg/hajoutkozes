package scws;


import  scws.SajatException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.internal.Throwables;


public class TomegTest {

	private static final Class SajatException = null;

	@Test
	public void test() throws scws.SajatException {
		
		assertNotNull(new Tomeg(1.0, "kg"));
	}
	
	@Test
	public void testHasonlitas() throws scws.SajatException {
		
		assertEquals(0,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(1.0, "kg")));
		assertNotEquals(0,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(2.0, "kg")));
		assertNotEquals(0,(new Tomeg(3.0, "kg")).compareTo(new Tomeg(2.0, "kg")));
		assertEquals(2,(new Tomeg(3.0, "kg")).compareTo(new Tomeg(1.0, "kg")));
		assertEquals(1,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(2.0, "kg")));
		assertEquals(0,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(1000.0, "g")));
		
		assertEquals(0,(new Tomeg(1000.0, "kg")).compareTo(new Tomeg(1.0, "t")));
		assertEquals(0,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(35.27396195, "oz")));
		assertEquals(0,(new Tomeg(1.0, "kg")).compareTo(new Tomeg(2.2046226218, "lb")));
		
		assertEquals(0,(new Tomeg(1000.0, "g")).compareTo(new Tomeg(35.27396195, "oz")));
		assertEquals(0,(new Tomeg(1000.0, "g")).compareTo(new Tomeg(2.2046226218, "lb")));
		assertEquals(0,(new Tomeg(1000000.0, "g")).compareTo(new Tomeg(1.0, "t")));
		
		assertEquals(0,(new Tomeg(1.0, "oz")).compareTo(new Tomeg(0.0625, "lb")));
		assertEquals(0,(new Tomeg(1.0, "t")).compareTo(new Tomeg(35273.96195, "oz")));
		
		assertEquals(0,(new Tomeg(1.0, "t")).compareTo(new Tomeg(2204.6226218, "lb")));
		
		// assertThrows(Exception.class()->(new Tomeg(1.0, "t")).compareTo(new Tomeg(2204.6226218, ""));
		// assertThrows(SajatException()-> new Tomeg(1.0, "t")).compareTo(new Tomeg(2204.6226218, ""));
	}
	
	@Test(expected = SajatException.class )
	public void isnull () throws SajatException {
		
		Tomeg egy = new Tomeg(1.0, "");

	}
		
		
	

}
