package scws;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class IdoTest {



	@Test
	public void initTest()  {
		
		assertNotNull(new Ido("12:16:22"));
	}
	
	//visszaadja a különbséget percekben
	@Test
	public void compareTest()  {
		
		assertEquals(2,(new Ido("12:16:22")).compareTo(new Ido("12:18:22")), 0.0);
		
	}
	//convertToMin felfele kerekit
	@Test
	public void convertToMinTest()  {
		
		assertEquals((double) 736.3666666666667, new Ido("12:16:22").convertToMin(),0.0);
		
	}
	// 0-24
	@Test
	public void testtimePlusTest()  {
		
		assertEquals("1:21:33",(new Ido("23:59:22")).timePlus(new Ido("1:22:11")));
		
	}
	//0-24
	@Test
	public void timeMinusTest()  {
		
		assertEquals("22:58:49",(new Ido("00:21:11")).timeMinus(new Ido("1:22:22")));
		
	}
	
	@Test
	public void trueAddTest() {
		assertEquals("124:22:18",(new Ido("23:59:22")).trueAdd(new Ido("100:22:56")));
		
		
	}
	

		

	
	@Test
	public void trueMinusTest_2() throws wrongTimeException {
		assertEquals("22:37:11",(new Ido("23:59:22")).trueMinus(new Ido("1:22:11")));
		
		
	}
	
	

}
