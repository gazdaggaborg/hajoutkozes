package scws;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class HajoTest {
	
	@Test
	public void testConstruct() throws FailedShipConstrictionException, SajatException {
		
		assertNotNull(new Hajo(new Tomeg(5,"t"),new Hossz(30,"m"), new Sebesseg(15,"km/h")));
	}
	
	@Test(expected = FailedShipConstrictionException.class )
	public void testInvalidConstruct () throws FailedShipConstrictionException, SajatException {
		
		Hajo ship = new Hajo(null,new Hossz(30,"m"), new Sebesseg(15,"km/h"));
	}
	
	@Test(expected = FailedShipConstrictionException.class )
	public void testInvalidConstruct2 () throws FailedShipConstrictionException, SajatException {
		
		Hajo ship = new Hajo(new Tomeg(5,"t"), null, new Sebesseg(15,"km/h"));
	}
	
	@Test(expected = FailedShipConstrictionException.class )
	public void testInvalidConstruct3 () throws FailedShipConstrictionException, SajatException {
		
		Hajo ship = new Hajo(new Tomeg(5,"t"),new Hossz(30,"m"), null);
	}
	
	@Test(expected = FailedShipConstrictionException.class )
	public void testInvalidConstruct4 () throws FailedShipConstrictionException {
		
		Hajo ship = new Hajo();
	}
}
