package scws;

import junit.framework.Assert;
import junit.framework.TestCase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class HosszTest {

	@Before
	public void setUp() throws Exception {
		Hossz hossz=new Hossz();
		assertNotNull(new Hossz());
	}
	
	@Test
	public void testCompareMethod() throws SajatException {
		assertEquals(0,(new Hossz(1.0, "m")).compareTo(new Hossz(1.0, "m")));
		assertEquals(0,(new Hossz(100.0, "cm")).compareTo(new Hossz(1.0, "m")));
		assertEquals(0,(new Hossz(1.0, "m")).compareTo(new Hossz(100.0, "cm")));
		assertEquals(0,(new Hossz(100.0, "cm")).compareTo(new Hossz(100.0, "cm")));
		assertEquals(0,(new Hossz(1000.0, "m")).compareTo(new Hossz(1.0, "km")));
		assertEquals(0,(new Hossz(2.54, "cm")).compareTo(new Hossz(1.0, "in")));
		assertEquals(0,(new Hossz(12.0, "in")).compareTo(new Hossz(1.0, "ft")));
		assertEquals(0,(new Hossz(1.0, "m")).compareTo(new Hossz(1.0, "m")));
		assertEquals(0,(new Hossz(3.0, "ft")).compareTo(new Hossz(1.0, "yd")));
		assertEquals(0,(new Hossz(1760, "yd")).compareTo(new Hossz(1.0, "mile")));
		assertEquals(0,(new Hossz(1852, "m")).compareTo(new Hossz(1.0, "nm")));
	}
	
	@Test (expected=SajatException.class)
    public void testInvalidHossz() throws SajatException{
	    new Hossz(100,"");
		new Hossz(3,"asds");
    }
	
	@Test
	public void addTest() throws SajatException {
		assertEquals(2.0,(new Hossz(1.0, "m")).addHossz(new Hossz(1.0, "m")),0);
		assertNotEquals(1.0,(new Hossz(1.0, "m")).addHossz(new Hossz(1.0, "m")),0);
		assertEquals(15.0,(new Hossz(5.0, "m")).addHossz(new Hossz(10.0, "m")),0);
	}
	
	@Test
	public void minusTest() throws SajatException {
		assertEquals(0.0,(new Hossz(1.0, "m")).minusHossz(new Hossz(1.0, "m")),0);
		assertNotEquals(1.0,(new Hossz(4.0, "m")).minusHossz(new Hossz(1.0, "m")),0);
		assertEquals(2.0,(new Hossz(3.0, "m")).minusHossz(new Hossz(1.0, "m")),0);
	    assertEquals(2.0,(new Hossz(1.0, "m")).minusHossz(new Hossz(3.0, "m")),0);
	}
	
	@Test
	public void multiplicationTest() throws SajatException {
		Hossz hossz=new Hossz(10,"m");
		hossz.multiplication(3.0);
		assertEquals(30.0,hossz.val,0);
		
		hossz.multiplication(5);
		assertEquals(150.0,hossz.val,0);
	}
	
	//@Test
	public void divisionTest() throws SajatException{
		Sebesseg sebesseg=new Sebesseg(10,"m/s");
		assertEquals(10.0, (new Hossz(10,"m").divisionSebesseg(sebesseg)),0);
	}
	
}

