package scws;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;


public class KalkulatorTest {

	private static Kalkulator k;
	@Before
	public void setUpBeforeClass() throws Exception {
		Tomeg t=new Tomeg(10000.0, "t");
		Hossz h=new Hossz(120.0, "m");
	    Sebesseg s=new Sebesseg(5.0, "m/s");
		Hajo sajat=new Hajo(t,h,s);
		k=new Kalkulator(sajat);
		
	}




	@Test
	public void testUtkozik_e() throws SajatException, FailedShipConstrictionException, FailedPositionConstructionException, wrongTimeException {
		
		//Utkozik, lassitanunk kell
		Tomeg tomeg=new Tomeg(8000.0, "t");
		Hossz hossz=new Hossz(1.0, "m");
	    Sebesseg sebesseg=new Sebesseg(1.0, "m/s");
		Hajo masik=new Hajo(tomeg,hossz,sebesseg);
		Pozicio pozicio=new Pozicio(new Hossz(2, "nm"),new Hossz(2,"nm") );
		int irany=190;
		assertEquals("� mehet,de lass�tsunk",k.utkozik_e(masik, pozicio, irany));
		
		//Nincs utkozes
		Tomeg tomeg1=new Tomeg(8000.0, "t");
		Hossz hossz1=new Hossz(1.0, "m");
	    Sebesseg sebesseg1=new Sebesseg(1.0, "m/s");
		Hajo masik1=new Hajo(tomeg1,hossz1,sebesseg1);
		Pozicio pozicio1=new Pozicio(new Hossz(10, "nm"),new Hossz(10,"nm") );
		int irany1=100;
		assertEquals("Nincs �tk�z�s!!",k.utkozik_e(masik1, pozicio1, irany1));
		
		//Utkozes, Mi mehetunk,de figyelj�nk r�
		Tomeg tomeg2=new Tomeg(11000.0, "t");
		Hossz hossz2=new Hossz(1.0, "m");
	    Sebesseg sebesseg2=new Sebesseg(1.0, "m/s");
		Hajo masik2=new Hajo(tomeg2,hossz2,sebesseg2);
		Pozicio pozicio2=new Pozicio(new Hossz(2, "nm"),new Hossz(2,"nm") );
		int irany2=220;
		assertEquals("Mi mehet�nk,de figyelj�nk r�",k.utkozik_e(masik2, pozicio2, irany2));
		
		//Ukozes, � mehet,de lass�tsunk
		Tomeg tomeg3=new Tomeg(500.0, "t");
		Hossz hossz3=new Hossz(1.0, "m");
	    Sebesseg sebesseg3=new Sebesseg(1.0, "m/s");
		Hajo masik3=new Hajo(tomeg3,hossz3,sebesseg3);
		Pozicio pozicio3=new Pozicio(new Hossz(2, "nm"),new Hossz(2,"nm") );
		int irany3=270;
		assertEquals("� mehet,de mi lass�tsunk",k.utkozik_e(masik3, pozicio3, irany3));

	}
	
	
	
	
	@Test
	public void converToIdoTest() {
		
		assertEquals("1:0:0",k.converttoIdo(3600));
		assertEquals("1:30:0",k.converttoIdo(5400));
		assertEquals("1:30:30",k.converttoIdo(5430));
	}

}
